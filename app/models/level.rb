class Level < ActiveRecord::Base
	has_many :questions
	belongs_to :compartment
end