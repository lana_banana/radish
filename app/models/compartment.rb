class Compartment < ActiveRecord::Base
	has_many :levels
	belongs_to :course
end
