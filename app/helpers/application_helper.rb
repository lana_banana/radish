module ApplicationHelper
	
	def header_class 
		content_for(:header_class) || ""
	end
end
