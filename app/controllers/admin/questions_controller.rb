class Admin::QuestionsController < ApplicationController

before_action :set_course
before_action :set_compartment
before_action :set_level

	def new
		@question = @course.compartments.level.question.new
	end

	def create
		@question = @course.compartments.level.question.new level_params
		if @question.save
			redirect_to edit_admin_question_path @question
		else
			render :new
		end
	end

	def edit
		@question = @course.compartments.level.question.find(params[:id])
	end

	def update
		@question = @course.compartments.level.find(params[:id])
		@question.update questions_params
		redirect_to edit_admin_question_path @question
	end

	def destroy
		@question = @course.compartments.level.question.find(params[:id])
		@question.destroy
		redirect_to edit_admin_question_path @question
	end

	private
	def set_course
		@course = Course.find(params[:course_id])
	end

	def set_compartment
		@compartment = @course.compartment.find(params[:compartment_id])
	end

	def set_level
		@level = @course.compartment.level.find(params[:level_id])
	end

	def question_params
		params.require(:question).permit(:body_question, :level_id)
	end
end
