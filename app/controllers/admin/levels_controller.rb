class Admin::LevelsController < ApplicationController
before_action :set_course
before_action :set_compartment

	def new
		@level = @course.compartments.level.new
	end

	def create
		@level = @course.compartments.level.new level_params
		if @level.save
			redirect_to edit_admin_level_path @level
		else
			render :new
		end
	end

	def edit
		@level = @course.compartments.level.find(params[:id])
	end

	def update
		@level = @course.compartments.level.find(params[:id])
		@level.update levels_params
		redirect_to edit_admin_level_path @level
	end

	def destroy
		@level = @course.compartments.level.find(params[:id])
		@level.destroy
		redirect_to edit_admin_level_path @level
	end

	private
	def set_course
		@course = Course.find(params[:course_id])
	end

	def set_compartment
		@compartment = @course.compartment.find(params[:compartment_id])
	end

	def level_params
		params.require(:level).permit(:level_name, :compartment_id)
	end


end
