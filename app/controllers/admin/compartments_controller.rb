class Admin::CompartmentsController < ApplicationController
		before_action :set_course

	def new
		@compartment = @course.compartments.new
	end

	def create
		@compartment = @course.compartments.new compartment_params
		if @compartment.save
			redirect_to edit_admin_course_path @course
		else
			render :new
		end
	end

	def edit
		@compartment = @course.compartments.find(params[:id])
	end

	def update
		@compartment = @course.compartments.find(params[:id])
		@compartment.update compartments_params
		redirect_to edit_admin_course_path @course
	end

	def destroy
		@compartment = @course.compartments.find(params[:id])
		@compartment.destroy
		redirect_to edit_admin_course_path @course
	end

	private
	def set_course
		@course = Course.find(params[:course_id])
	end

	def compartment_params
		params.require(:compartment).permit(:compartment_name, :course_id)
	end

end
