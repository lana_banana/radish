class QuestionsController < ApplicationController
	
	before_action :set_level
	def index
		@question = @course.compartment.level.question.all 
	end

	def show
		@question = @course.compartment.level.question.find(params[:id])
	end

	private
	def set_course
		@course = Course.find(params[:course_id])
	end
	def set_compartment
		@compartment = @course.compartment.find(params[:compartment_id])
	end
	def set_level
		@level = @course.compartment.level.find(params[:level_id])
	end
end
