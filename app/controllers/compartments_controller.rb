class CompartmentsController < ApplicationController
	before_action :set_course
	def index
		@compartments = @course.compartments.all 
	end

	def show
		@compartment = @course.compartments.find(params[:id])
	end

	private
	def set_course
		@course = Course.find(params[:course_id])
	end
	def compartment_params 
		params.require(:compartment).permit(:compartment_name)
	end
end
