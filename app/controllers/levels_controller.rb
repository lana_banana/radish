class LevelsController < ApplicationController
	
	before_action :set_compartment
	def index
		@levels = @compartment.levels.all 
	end

	def show
		@level = @compartment.levels.find(params[:id])
	end

	def check

	end
	private
	def set_compartment
		@compartment = Compartment.find(params[:compartment_id])
	end
end
