class DashboardController < ApplicationController
	before_action :authenticate_user!, :set_current_user

	def index
		@me
	end

	def edit
		@me
	end

	def update
		if @me.update(user_params)
			redirect_to :action => :index
		else
			render :edit
		end
	end

	private
	def set_current_user
		@me = current_user if user_signed_in?
	end

	/def user_params
		params.require(:user).permit(:name, :surname, :dob, :gender, :email, :avatar)
	end/
end
