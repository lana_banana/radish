class CreateCompartments < ActiveRecord::Migration
  def change
    create_table :compartments do |t|
      t.string :compartment_name
      t.timestamps
    end
  end
end
