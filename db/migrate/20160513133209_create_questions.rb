class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
    	t.string :body_question
      t.timestamps
    end
  end
end
