class ChangeBodyQuestionTypeInQuestions < ActiveRecord::Migration
  def change
  	change_column :questions, :body_question, :text
  end
end
